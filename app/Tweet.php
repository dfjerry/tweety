<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Tweet extends Model
{

    protected $guarded = [];
    public function user(){
        return $this->belongsTo(User::class);
    }
    public function scopeWithLikes(Builder $query){
        $query->leftJoinSub(
            'select tweet_id, sum(liked) likes, sum(!liked) dislikes from likes group by tweet_id',
            'likes',
            'tweets.id',
            'likes.tweet_id'

        );
    }
    public function isLikeBy(User $user)
    {
        return $this->likes()
            ->where('user_id', $user->id)
            ->where('liked', true)
            ->exists();
    }

    public function likes()
    {
        return $this->hasMany(Like::class);
    }

    public function isDislikeBy(User $user)
    {

        return $this->likes()
            ->where('user_id', $user->id)
            ->where('liked', false)
            ->exists();
    }

    public function dislike($user = null)
    {
        return $this->like($user, false);
    }

    public function like($user = null, $liked = true)
    {
        $this->likes()->updateOrCreate([
            'user_id' => $user ? $user->id : auth()->id(),
        ], [
            'liked' => $liked
        ]);
    }
}
