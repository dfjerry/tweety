<div class="flex p-4 border border-gray-400">
    <div class="mr-4 flex-shrink-0">
        <a href="{{$tweet->user->path()}}">
        <img src="{{$tweet -> user -> getAvatarAitribute()}}" alt="" class="rounded-full mr-2" style="width: 40px; height: 40px">
        </a>
    </div>
    <div>
        <h5 class="font-bold mb-4">
            <a href="{{$tweet->user->path()}}">
            {{ $tweet -> user -> name }}
            </a>
        </h5>
        <p class="text-sm">
            {{ $tweet -> body }}
        </p>
        <div>
            <div class="flex">
                <form action="/tweets/{{$tweet->id}}/like" method="post">
                    @csrf
                    <div class="{{$tweet->isLikeBy(current_user()) ? 'text-blue-500' : 'text-gray-500'}}">
                        <button ><i class="far fa-thumbs-up"></i>{{$tweet->likes ?: 0}}</button>
                    </div>
                </form>
                <form action="/tweets/{{$tweet->id}}/like" method="post">
                    @csrf
                <div class="{{$tweet->isDislikeBy(current_user()) ? 'text-blue-500' : 'text-gray-500'}}">
                    <button><i class="far fa-thumbs-down"></i>{{$tweet->dislikes?: 0}}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
