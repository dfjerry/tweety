<ul>
    <li><a href="{{url('/tweets')}}" class="font-bold text-lg mb-4 block">Home</a></li>
    <li><a href="/explore" class="font-bold text-lg mb-4 block">Explore</a></li>
    <li><a href="{{url('/profiles', auth()->user())}}" class="font-bold text-lg mb-4 block">Profile</a></li>
    <li>
        <form action="/logout" method="POST" >
            @csrf
            <button type="submit">Logout</button>
        </form>
    </li>
</ul>
