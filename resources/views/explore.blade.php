@extends('components.app')

@section('content')
    <div class="lg:flex lg:justify-between ">
        <div class="lg:w-32">
            @include("_sidebar-links")
        </div>
        <div class="mb-6 relative " style="width: 950px">
            <img class="mb-6" src="https://picsum.photos/950/233" alt="">
            <div>
                @foreach($users as $user)

                    <a href="{{$user->path()}}" class="flex items-center mb-5">

                        <img src="{{$user->avatar}}" alt="{{$user->username}}">
                        <div>
                            <h4 class="font-bold">{{'@' . $user->username}}</h4>
                        </div>
                    </a>
                    @endforeach
                {{$users->links()}}
            </div>


        </div>
        <div class="lg:w-1/6 bg-blue-100 rounded-lg p-4">
            @include("_friend-list")
        </div>
    </div>
@endsection
