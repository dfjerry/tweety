@extends('components.app')

@section('content')
    <div class="lg:flex lg:justify-between ">
        <div class="lg:w-32">
            @include("_sidebar-links")
        </div>
        <div class="mb-6 relative " style="width: 950px">
            <img class="mb-6" src="https://picsum.photos/950/233" alt="">
            <div class="flex justify-between items-center mb-6">
                <div>
                    <h2 class="font-bold text-2xl mb-2">{{$user->name}}</h2>
                    <p class="text-sm">Joined {{$user->created_at->diffForHumans()}}</p>
                </div>

                <div class="flex">

                    @if(current_user()->is($user))
                        <a href="{{$user->path('edit')}}" class=" rounded-full text-black text-xs  rounded-lg shadow py-4 px-4">Edit Profile</a>

                    @endif
                    <x-follow-button :user="$user"/>
                </div>
            </div>
            <img src="{{$user->avatar}}" alt="" class="rounded-full mr-2 absolute " style="width: 150px; left: calc(50% - 75px); top: 150px">
            <p class="text-sm">Text messaging, or texting, is the act of composing and sending electronic messages,
                typically consisting of alphabetic and numeric characters, between two or more
                users of mobile devices, desktops/laptops, or other type of compatible computer. Wikipedia
            </p>
            <hr/>
            @include('_timeline', [
            'tweets'=>$user->tweets
    ])
        </div>

        <div class="lg:w-1/6 bg-blue-100 rounded-lg p-4">
            @include("_friend-list")
        </div>
    </div>
@endsection
