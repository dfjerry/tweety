@extends('components.app')

@section('content')
    <div class="lg:flex lg:justify-between ">
        <div class="lg:w-32">
            @include("_sidebar-links")
        </div>
        <div class="mb-6 relative " style="width: 950px">
            <img class="mb-6" src="https://picsum.photos/950/233" alt="">
            <img src="{{$user->avatar}}" alt="" class="rounded-full mr-2 absolute " style="width: 150px; left: calc(50% - 75px); top: 150px">
            <form class="mt-20" method="post" action="{{$user->path()}}" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div>
                    <div class="mb-6">
                        <label for="name" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                            Name
                        </label>
                        <input type="text" value="{{$user->name}}" class="border border-gray-400 p-2 w-full" name="name" id="name" required>
                        @error('name')
                        <p class="text-red-500 text-xs mt-2">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <label for="username" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                            Username
                        </label>
                        <input type="text" value="{{$user->username}}" class="border border-gray-400 p-2 w-full" name="username" id="username" required>
                        @error('username')
                        <p class="text-red-500 text-xs mt-2">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <div>
                            <label for="avatar" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                                Avartar
                            </label>
                            <input type="file" class="border border-gray-400 p-2 w-full" name="avatar"  required>
                            @error('avatar')
                            <p class="text-red-500 text-xs mt-2">{{$message}}</p>
                            @enderror
                        </div>
                    </div>
                    <div class="mb-6">
                        <label for="email" class="block mb-2 uppercase font-bold text-xs text-gray-700">
                            Email
                        </label>
                        <input value="{{$user->email}}" type="email" class="border border-gray-400 p-2 w-full" name="email" id="email" required>
                        @error('email')
                        <p class="text-red-500 text-xs mt-2">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <label for="password">
                            Password
                        </label>
                        <input value="" type="password" class="border border-gray-400 p-2 w-full" name="pasword" id="password" >
                        @error('password')
                        <p class="text-red-500 text-xs mt-2">{{$message}}</p>
                        @enderror
                    </div>
                    <div class="mb-6">
                        <button type="submit" class="bg-blue-400 text-white roundedd py-2 px-4 hover:bg-blue-500">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <div class="lg:w-1/6 bg-blue-100 rounded-lg p-4">
            @include("_friend-list")
        </div>
    </div>
@endsection
