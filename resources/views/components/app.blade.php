<x-master>
    <section class="px-8">
        <main class="container mx-auto">
            @yield('content')
        </main>
    </section>
</x-master>
