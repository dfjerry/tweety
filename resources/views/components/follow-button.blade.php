@if(! current_user()->is($user))
    <form action="{{route('follow', $user->username)}}" method="POST">
        @csrf

        <button type="submit" class="rounded-full bg-blue-500 rounded-lg shadow text-white text-xs py-4 px-4">{{auth()->user()->following($user) ? 'Unfollow Me' : 'Follow Me'}}</button>
    </form>

    @endif

