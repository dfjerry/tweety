<div class="border border-blue-400 rounded-lg px-8 py-6 mb-6">
    <form method="POST" action="/tweets">
        @csrf
        <textarea name="body" class="w-full" placeholder="What's up docs?" ></textarea>
        <hr class="my-4">
        <footer class="flex justify-between">
            <img src="{{auth()->user()->getAvatarAitribute()}}" alt="" class="rounded-full mr-2 " style="width: 40px; height: 40px">

            <button class="bg-blue-400 rounded-lg shadow py-4 px-4"
                    type="submit">
                Publish
            </button>
        </footer>
    </form>
    @error('body')
    <p class="text-red-500 text-sm">{{$message}}</p>
    @enderror
</div>

