<h3 class="font-bold text-xl mb-4">Following</h3>
<ul>
    @forelse(auth()->user()->follows as $user)
        <li class="mb-4">
            <div class="flex  items-center">
                <a href="{{route('profile', $user)}}">
                    <div class="flex">
                        <img src="{{$user->getAvatarAitribute()}}"
                                            alt=""
                                            class="rounded-full mr-4"
                                            style="width: 40px; height: 40px"
                        >
                        {{$user->name}}
                    </div>

                </a>
            </div>
        </li>
        @empty
        <li>No friend yet!</li>
    @endforelse
</ul>
